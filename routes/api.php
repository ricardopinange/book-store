<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/login', function () {
    return response()->json(['message' => 'Unauthenticated.'], 401);
})->name('login');

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group(['middleware' => ['auth:api']], function() {

    Route::group(['prefix' => 'user'], function () {
        Route::post('register', 'UserController@register');
    });

    Route::group(['prefix' => 'book'], function () {
        Route::get('list', 'BookController@index');
        Route::post('insert', 'BookController@store');
        Route::get('edit/{id}', 'BookController@edit');
        Route::put('update/{id}', 'BookController@update');
        Route::delete('delete/{id}', 'BookController@destroy');
    });

});


