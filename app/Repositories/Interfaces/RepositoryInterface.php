<?php
namespace App\Repositories\Interfaces;

Interface RepositoryInterface {

    public function all();
    public function find($id);
    public function findLastOneByFilter(array $filter);
    public function store($data);
    public function update($data, $id);
    public function destroy($id);

}
