<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Book;
use Illuminate\Http\Request;

class BookRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = Book::select();

        if (isset($this->request->search)) {
            $search = $this->request->search;
            $query->where(function ($query) use ($search) {
                $query->whereRaw("UPPER(name) LIKE '%" . strtoupper($search) . "%'")
                    ->orWhere('isbn', 'LIKE', '%' . $search . '%');
            });
        }

        if (isset($this->request->start_value)) {
            $query->where('value', '>=', $this->request->start_value);
        }

        if (isset($this->request->end_value)) {
            $query->where('value', '<=', $this->request->end_value);
        }

        if (isset($this->request->per_page)) {
            $per_page = in_array($this->request->per_page, [12,24,36]) ? $this->request->per_page : 12;
        }

        $data = $query->orderBy('name')->paginate($per_page ?? 12);

        return $data;
    }

    public function find($id)
    {
        return Book::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return Book::where($filter)->first();
    }

    public function store($data)
    {
        Book::create($data);
        return self::findLastOneByFilter($data);
    }

    public function update($data, $id)
    {
        Book::find($id)->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
    }
}
