<?php

namespace App\Console\Commands;

use App\Models\User;
use Hamcrest\AssertionError;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class GenerateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userName = $this->ask("'What is the username?");
        $email = $this->ask("What is the user's email?");
        $password = $this->ask("What is the user's password?");
        
        $user = User::query()->create([
            'name' => $userName,
            'email' => $email,
            'password' => Hash::make($password)
        ]);

        $this->info('Successfully created user');

        // $token = $user->createToken('token');
        // $this->info('Successfully created token: ' . $token->plainTextToken );
    }
}