<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UserController extends Controller
{

    /**
     * Store a newly created resource in storage.
     */
    public function register(UserRequest $request)
    {
        try {
            $user = User::where(['email' => $request->email])->first();

            if (!empty($user)) {
                throw new \Exception('User already registered', Response::HTTP_BAD_REQUEST);
            }

            $data = User::query()->create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Registration successfully added',
                    'data' => $data
                ],
                Response::HTTP_OK
            );
        } catch(\Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => false,
                    'data' => $e->getMessage()
                ],
                $this->statusResponse($e->getCode())
            );
        }
    }
}
