<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Is http response valid?
     *
     * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
     *
     * @param int $code
     * @return bool
     */
    public function statusResponse($code)
    {
        $intCode = intval($code);
        return $intCode >= 100 && $intCode < 600 && is_int($intCode)
            ? $intCode
            : Response::HTTP_INTERNAL_SERVER_ERROR;
    }

}
