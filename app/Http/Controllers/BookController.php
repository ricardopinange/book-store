<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Repositories\BookRepository;
use App\Http\Requests\BookRequest;

class BookController extends Controller
{
    private $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = $this->bookRepository->all();
            return response()->json(
                [
                    'success' => true,
                    'message' => true,
                    'data' => $data
                ],
                Response::HTTP_OK
            );
        } catch(\Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => false,
                    'data' => $e->getMessage()
                ],
                $this->statusResponse($e->getCode())
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BookRequest $request)
    {
        try {
            $data = $this->bookRepository->store($request->all());
            return response()->json(
                [
                    'success' => true,
                    'message' => 'Registration successfully added',
                    'data' => $data
                ],
                Response::HTTP_OK
            );
        } catch(\Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => false,
                    'data' => $e->getMessage()
                ],
                $this->statusResponse($e->getCode())
            );
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $data = $this->bookRepository->find($id);
            return response()->json(
                [
                    'success' => true,
                    'message' => true,
                    'data' => $data
                ],
                Response::HTTP_OK
            );
        } catch(\Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => false,
                    'data' => $e->getMessage()
                ],
                $this->statusResponse($e->getCode())
            );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BookRequest $request, string $id)
    {
        try {
            $book = $this->bookRepository->find($id);

            if (empty($book)) {
                throw new \Exception('Register not found', Response::HTTP_BAD_REQUEST);
            }

            $data = $this->bookRepository->update($request->all(), $id);
            return response()->json(
                [
                    'success' => true,
                    'message' => 'Registration successfully updated',
                    'data' => $data
                ],
                Response::HTTP_OK
            );
        } catch(\Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => false,
                    'data' => $e->getMessage()
                ],
                $this->statusResponse($e->getCode())
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $book = $this->bookRepository->find($id);

            if (empty($book)) {
                throw new \Exception('Register not found', Response::HTTP_BAD_REQUEST);
            }

            $this->bookRepository->destroy($id);
            return response()->json(
                [
                    'success' => true,
                    'message' => 'Registration successfully deleted',
                    'data' => []
                ],
                Response::HTTP_OK
            );
        } catch(\Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => false,
                    'data' => $e->getMessage()
                ],
                $this->statusResponse($e->getCode())
            );
        }
    }
}
